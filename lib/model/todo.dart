import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

part 'todo.g.dart';

class ToDoModel with ChangeNotifier {
  void getToDo(List<ToDo> list) async {
    var box = await Hive.openBox<ToDo>('test');
    for (ToDo value in box.values) {
      list.add(value);
    }
    notifyListeners();
  }

  void addToDo(String text) async {
    var box = await Hive.openBox<ToDo>('test');
    final value = ToDo(todoText: text);
    box.add(value);
    notifyListeners();
  }
}

@HiveType(typeId: 0)
class ToDo {
  @HiveField(0)
  String? todoText;
  @HiveField(1)
  bool isDone;

  ToDo({
    this.todoText,
    this.isDone = false,
  });
}
