import 'package:flutter/material.dart';
import 'package:todo_app/constants/colors.dart';

import '../model/todo.dart';

class TodoItemWidget extends StatelessWidget {
  const TodoItemWidget(
      {super.key, required this.toDo, this.OnToDoChanged, this.OnDeleteItem});

  final ToDo toDo;
  final OnToDoChanged;
  final OnDeleteItem;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: ListTile(
        onTap: () {
          OnToDoChanged(toDo);
        },
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        contentPadding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        tileColor: Colors.white,
        leading: Icon(
          toDo.isDone ? Icons.check_box : Icons.check_box_outline_blank,
          color: myBlue,
        ),
        title: Text(
          toDo.todoText!,
          style: TextStyle(
              color: myBlack,
              decoration: toDo.isDone
                  ? TextDecoration.lineThrough
                  : TextDecoration.none),
        ),
        trailing: Container(
          height: 45,
          width: 45,
          decoration: BoxDecoration(
              color: myRed, borderRadius: BorderRadius.circular(5)),
          child: IconButton(
              onPressed: () {},
              iconSize: 20,
              color: myWhite,
              icon: Icon(Icons.delete)),
        ),
      ),
    );
  }
}
