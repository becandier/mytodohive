import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/constants/colors.dart';
import 'package:todo_app/widgets/todo_item.dart';

import '../model/todo.dart';

class HomeView extends StatefulWidget {
  HomeView({super.key});

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  List<ToDo> _foundToDo = [];
  final _toDoController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  void update() {
    setState(() {
      ToDoModel().getToDo(_foundToDo);
    });
  }

  @override
  Widget build(BuildContext context) {
    setState(() {
      ToDoModel().getToDo(_foundToDo);
    });
    return Scaffold(
      backgroundColor: myWhite,
      appBar: _myAppBar(),
      drawer: Drawer(
        child: Center(
            child: Text('Sharafuddin developer \n \n' +
                'extends Comrades devs' +
                '\n \n \n \n \n \n \n \n \n ' +
                '@imanov.sh')),
      ),
      body: Stack(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
            child: Column(
              children: <Widget>[
                SearchWidget(),
                Expanded(
                    child: ListView(
                  children: [
                    ParagraphTextWidget(),
                    _foundToDo.isEmpty
                        ? Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text('Задач нет \n \nПопробуйте обновить'),
                              TextButton(
                                  onPressed: update,
                                  child: Text(
                                    'Обновить',
                                    style: TextStyle(color: myBlue),
                                    textAlign: TextAlign.start,
                                  ))
                            ],
                          )
                        : Container(),
                    for (ToDo toDo in _foundToDo)
                      TodoItemWidget(
                        toDo: toDo,
                        OnToDoChanged: _changedToDo,
                        OnDeleteItem: _deleteItem,
                      ),
                  ],
                ))
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 20),
            alignment: Alignment.bottomCenter,
            child: Row(
              children: [
                Expanded(
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    margin: EdgeInsets.only(bottom: 20, right: 20, left: 20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(0.0, 0.0),
                          blurRadius: 1.0,
                          spreadRadius: 0.0,
                        )
                      ],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: TextField(
                      controller: _toDoController,
                      decoration: InputDecoration(
                          hintText: 'Добавить задачу',
                          border: InputBorder.none),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(bottom: 20, right: 20),
                  child: ElevatedButton(
                    onPressed: () {
                      _addToDoItem(_toDoController.text);
                    },
                    style: ElevatedButton.styleFrom(
                        primary: myBlue,
                        minimumSize: Size(60, 60),
                        elevation: 2),
                    child: Text(
                      '+',
                      style: TextStyle(fontSize: 40),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  void _changedToDo(ToDo toDo) {
    setState(() {
      toDo.isDone = !toDo.isDone;
    });
  }

  void _deleteItem(String id) {
    setState(() {
      // toDoList.removeWhere((item) => item.id == id);
    });
  }

  void _addToDoItem(String text) {
    setState(() {
      ToDoModel().addToDo(text);
      ToDoModel().getToDo(_foundToDo);
    });
    _toDoController.clear();
  }

  void _runFilter(String enteredKeyword) {
    List<ToDo> results = [];

    // if (enteredKeyword.isEmpty) {
    //   results = toDoList;
    // } else {
    //   results = toDoList
    //       .where((item) => item.todoText!
    //           .toLowerCase()
    //           .contains(enteredKeyword.toLowerCase()))
    //       .toList();
    // }

    setState(() {
      _foundToDo = results;
    });
  }

  AppBar _myAppBar() {
    return AppBar(
      elevation: 0,
      backgroundColor: myWhite,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Container(
              height: 35,
              width: 35,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  image: DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage('assets/images/Dagestan.png')))),
        ],
      ),
      leading: Builder(
        builder: (context) {
          return InkWell(
              onTap: () {
                Scaffold.of(context).openDrawer();
              },
              borderRadius: BorderRadius.circular(8),
              child: Icon(
                Icons.menu,
                color: myBlack,
              ));
        },
      ),
    );
  }

  Widget SearchWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: TextField(
          onChanged: (value) => _runFilter(value),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(0),
            prefixIcon: Icon(
              Icons.search,
              color: myBlack,
              size: 20,
            ),
            prefixIconConstraints: BoxConstraints(maxHeight: 20, minWidth: 25),
            border: InputBorder.none,
            hintText: 'Поиск',
          )),
    );
  }
}

class ParagraphTextWidget extends StatelessWidget {
  const ParagraphTextWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 50, bottom: 20),
      child: Text(
        'Ваши задачи',
        style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
      ),
    );
  }
}
